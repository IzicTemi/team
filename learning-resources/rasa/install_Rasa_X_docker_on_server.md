
## Steps to create a DigitalOcean droplet with Docker installed
https://gitlab.com/tangibleai/team/-/blob/master/learning-resources/digitalocean/new_digitalocean_droplet_with_docker.md

## Install Rasa X docker on server
https://rasa.com/docs/rasa-x/installation-and-setup/install/docker-compose

Download the install script on the server:
```bash
curl -sSL -o install.sh https://storage.googleapis.com/rasa-x-releases/0.39.3/install.sh
```

To install all of the files into the default folder, /etc/rasa, run:
```bash
sudo bash ./install.sh
```
Start up Rasa X and wait until all containers are running (-d will run Rasa X in the background):
```bash
ls
cd /etc/rasa
ls
sudo docker-compose up -d
```

Set your admin password with this command:
```bash
cd /etc/rasa
sudo python3 rasa_x_commands.py create --update admin me <PASSWORD>
```

#### connect with Gitlab

```bash
ssh-keygen -t rsa -b 4096 -f git-deploy-key
```


root@tangible-rasa:~#
```bash
cd /etc/rasa
cat git-deploy-key.pub
```

- copy public key

- navigate to your gitlab repo. Go to settins --> Deploy keys --> expand

- give title and paste key

**don't forget to Grant write permissions to this key**

*If key is already user for another project look for _Privately accessible deploy keys_ and enable desired key*

to get api_token go to new Rasa X --> models --> upload model and copy token from command
*change url to new ip

#### verify integration
```bash
curl --request POST\
     --url http://167.99.165.173/api/projects/default/git_repositories?api_token=5eca967174c5bc1a544947a9293a9a53b325bb11\
     --header 'Content-Type: application/json'\
     --data-binary @repository.json

```

###### restart docker

```bash
sudo docker-compose down -v
sudo docker-compose up -d
```

#### navigate to rasa x
tangible-rasa.qary.ai -> Training -> Add Model -> Train Model
