# DrawIO

Skip to  [Install](#install-drawio) if you already know what `drawio` is.

## Raster vs Vector diagrams

Raster images are the typical image files that you are used to, JPG and PNG are the most common formats.
PDFs are also sometimes just JPGs wrapped with some proprietary markup for Adobe Acrobat.

Vector graphics are diagrams that you can easily edit and reuse later.
And they are infinitely scalable.
You can zoom in or out and they still look sharp.
In a vector graphic image all objects are represented as vectors, kind-of like a flattened 3-D model of your image.
It's great for diagrams, because you can move around and zoom around in a 2-D vector diagram just like you can in a 3-D world created with Blender or a CAD tool.
Basically vector graphics is CAD for 2-D diagrams.

Draw IO is an open source Javascript application that lets you create these vector diagrams in `.drawio` files that you (or anyone else) can reuse later.
It's the only way to really open source your artwork or diagrams.
And it has a lot of boilerplate examples that can get you started on things like org charts, dialog trees, and flow diagrams.
There are even some open source cloud network architecture diagrams with icons for the big cloud services.

Visio and InksScape are comparable apps.
Inkscape can save and edit SVG files natively.
Visio has a proprietary format, so it can only export to SVG.
DrawIO's file format is open and just XML/text.
So it can even be automatically embedded in PNG files.
So your diagrams can be viewable by anyone anywhere and reusable by anyone with `drawio-desktop` installed.

## Install DrawIO

DrawIO is a javascript app, so it can run in your browser and there are paid services and free webapps that will let you creat diagrams without installing anything.
But it's much easier to work with `.drawio` files on your computer if you install the drawio-desktop app.

### Debian Linux

```sh
sudo apt install -y wget curl
curl -s https://api.github.com/repos/jgraph/drawio-desktop/releases/latest | grep browser_download_url | grep '\.deb' | cut -d '"' -f 4 | wget -i -
sudo apt -f install ./drawio-amd64-*.deb
```

### Windows

You'll need to run this in a git-bash terminal for the automagic download to work:

```sh
curl -s https://api.github.com/repos/jgraph/drawio-desktop/releases/latest \
    | grep browser_download_url \
    | grep -E '"[^"]*\-windows\-installer\.exe"' \
    | cut -d '"' -f 4 \
    | wget -i -
chmod +x *-windows-installer.exe
./draw.io*-windows-installer.exe
```

If you're stuck on a machine without git-bash installed you will have to manually browse to the jgraph repo to find the binary install appropriate for your environment.

### Mac

```sh
sudo apt install -y wget curl
curl -s https://api.github.com/repos/jgraph/drawio-desktop/releases/latest | grep browser_download_url | grep '\.dmg"' | cut -d '"' -f 4 | wget -i -
```

Then manually launch the dmg (disk image) file and drag the DrawIO app into your Applications Folder (directory).
