# Thank you to our Contributors

- Maria Dyshell (@maria_tangibleai): various chatbot and nonprofit business sector workshops
- Hobson Lane (@hobs): various technical HowTo videos and READMEs
- Dwayne Negron (@dwayne.negron1): virtually everything you see here!
- John May (@jmayjr): tutorial videos and software development for qary and tanbot/internbot
- Jose Robins (@jorobins): FactQuest/BlankPage/QuizBot/YesNoBot
- Jon Sundin (@jonsundin)
- Aditi
