# Algo VPN Deployment to DigitalOcean Server
The steps below will outline how to set up a DigitalOcean Droplet (virtual machine) and deploy the Algo VPN to the server.

### Create API Token

**1. Create API Token (see [link](https://docs.digitalocean.com/reference/api/create-personal-access-token/)):**
1. From DigitalOcean Navigation Bar (left side of screen):
  - Select `Account`->`API` (takes you to Applications & API screen)
  - Under `Tokens/Keys` select `Generate New Token`
    - Enter Token Name
    - Check Write box (allows token writing privileges)
    - Click `Generate Token` button
 - Record the personal access token before leaving the page because you cannot access it again This will be used during deployment of the Algo VPN.

2. **Install Algo and it's dependencies (see [link](https://github.com/trailofbits/algo)):**
  - clone algo repo:\
`git clone https://github.com/trailofbits/algo.git`
  - Create virtual environment and install dependencies (see link above for creating a virtual environment on different systems). Once virtenv is installed, run:
  ```
  python3 -m virtualenv --python="$(command -v python3)" .env &&
      source .env/bin/activate &&
      python3 -m pip install -U pip virtualenv &&
      python3 -m pip install -r requirements.txt
  ```

3. **Add users and complete configuration:**
  - Got to algo/config/cfg:
    - Add all users expected to access VPN
    - Check remaining configurations
    - Save file

4. **Deploy Server! (see [link](https://github.com/trailofbits/algo/blob/master/docs/cloud-do.md))**
  - From terminal:
    - cd into cloned repository directory
    - launch algo deployment:\
    `./algo`
      ```
      [VPN server name prompt]
      What provider would you like to use?
      Enter the number of your desired provider
      ```
      >Type 1 for DigitalOcean.

      ```
      Name the vpn server [algo]
      ```
      >This is the name of the Droplet that will be created on DigitalOcean. If you type nothing, default name is `algo`.

      ```
      [Cellular On Demand prompt]
      Do you want macOS/iOS clients to enable "Connect On Demand" when connected to cellular networks?
      n
      ```

      ```
      [Wi-Fi On Demand prompt]
      Do you want macOS/iOS clients to enable "Connect On Demand" when connected to Wi-Fi?
      n
      ```

      ```
      [DNS adblocking prompt]
      Do you want to enable DNS ad blocking on this VPN server?
      n
      ```

      ```
      [SSH tunneling prompt]
      Do you want each user to have their own account for SSH tunneling?
      n
      ```

      ```
      [cloud-digitalocean : pause]
      Enter your API token. The token must have read and write permissions (https://cloud.digitalocean.com/settings/api/tokens):
      (output is hidden):
      ```
      >Enter your API Token created on DigitalOcean in **Step 1**.

      ```
      [cloud-digitalocean : pause]
      What region should the server be located in?
      ```
      >Choose the region where you want the server to be located. I chose ` 8. nyc3     New York 3`

    After finishing the above prompts, algo should have created a Droplet (VM) on Digital Ocean. The Droplet uses the latest supported version of Ubuntu by default.

    The terminal should display a message if all above steps worked:
       ```
       "\"#                         Congratulations!                            #\",
       "\"#                     Your Algo server is running.                     #\",
       ```
    Try to ssh into your Droplet using the information within the message. It should look similar to:\
    `"    \"#      Shell access: ssh -F configs ...`

5. **Configure Droplet:**
  - On DigitalOcean, click Droplet name to go to Droplet Settings.
    - Go to 'Resize' and turn off Droplet (upper right-hand side of screen).
    - Choose the preferred Droplet type, the click 'Resize' at bottom of screen.
    - Turn Droplet back on (if needed).

6. **Sharing Keys for Users to access VPN:**
  - When Algo VPN was deployed, it created .config files with security keys for the different Users to access the VPN. To locate and share these files, navigate to the cloned algo repository folder on your local machine:
    - `algo/configs/<ip-address>/wireguard/<user.conf>` where:
      - `<ip-address>` is the IP address of the Droplet, assigned by the Algo VPN at time of deployment.
      - `<user.conf>` is the config file to share with `user` assigned in **Step 3**.
