# Quote Recommender
**Tangible AI Intern Capstone Project by Billy Horn**\
https://gitlab.com/billy-horn/quote-recommender


## Introduction
Recommendation engines are ubiquitous in the artificial intelligence industry. A large majority of these systems are utilized for improving a consumer's experience with a product or service, ultimately benefitting the company. This project instead aims to harness the power of recommendation engines to benefit the end-user by attempting to predict and present them with quotes they will like in hopes of inspiring, boosting mood or just having fun.

This project's recommendation engine is built upon a classification predictive model, and similar to other language-based content recommenders, it uses NLP to construct features for the model. The system is deployed to a CLI program for user interfacing, employing a custom chatbot framework to deliver quotes and receive feedback from the user. The feedback is then used to train the model for future predictions.

The following report will outline the data transformations, modeling approaches and results, chatbot framework, as well as future steps for improvements. The stretch goal for this project is for the recommendation engine to be incorporated into the open source chatbot framework - [qary](https://gitlab.com/tangibleai/qary) - however, only the MVP is represented here.

## Table of Contents
- [Data](#data)
  - [Data Dictionary](#data-dictionary)
- [Modeling](#modeling)
  - [Preprocessing and Feature Engineering](#preprocessing-and-feature-engineering)
  - [Methods of Analysis](#methods-of-analysis)
  - [Results](#results)
- [CLI Program](#cli-program)
- [Future Work](#future-work)
- [Conclusions](#conclusions)
- [Sources](#sources)
- [Appendix](#appendix)

## Data
The data for this project was sourced from the [Quote Recommendation](https://github.com/MartinStyk/quotes-recommender) project by Vojtech Hlavka, David Luptak and Martin Styk. The data was scraped from www.brainyquote.com, and consists of over nine-thousand quotes with their respective authors and categroies. The quotations originate from a wide spectrum of famous icons, including those from Eminem to Leonardo Da Vinci, Donald Trump to Buddha, and everyone in between.

Although the data dictionary below shows additional columns for reference, the original data set contained only the *text*, *author* and *categories* columns. More comprehensive descriptions for the remaining columns can be found in the [*Preprocessing and Feature Engineering*](#preprocessing-and-feature-engineering) section below.

#### Data Dictionary
|**Feature**|**Type**|**Description**|
|---|---|---|
|**text**|*object*|The original text from the quote.|
|**author**|*object*|The author credited to the quote.|
|**categories**|*object*|Categories attributed to the quote, provided by www.brainyquotes.com.|
|**tags**|*object*|The transformed text of the 'categories' column, which separates words by white space in lieu of commas.|
|**quote+author**|*object*|A combination of the text from the 'text' and 'author' columns.|
|**quote+tags**|*object*|A combination of the text from the 'text' and 'tags' columns.|
|**quote+author+tags**|*object*|A combination of the text from the 'text', 'author' and 'tags' columns.|
|**quote_word_count**|*integer*|A sum of the total number of words contained in the 'text' column.|
|**sentiment_score**|*float*|A sentiment score obtained from a sentiment analysis on the 'text' column.|
|**quote_index**|*integer*|A duplicate index of the quote from the original data set.|
|**user_id**|*integer*|An identification number of the user from which the value in the 'user_feedback' column was gathered.|
|**neg_sim**|*float*|A similarity score of the text in the 'text' column compared to all other quotes that were disliked by the user.|
|**pos_sim**|*float*|A similarity score of the text in the 'text' column compared to all other quotes that were liked by the user.|
|**user_feedback**|*integer*|Feedback from the user in the 'user_id' column indicated by a 0 or 1 for if they disliked or liked the quote, respectively.|
>The above data dictonary represents all columns of the *global_history* table. All aspects of the engine and CLI program use different permutations of this table.

## Modeling

#### Preprocessing and Feature Engineering
The original data set was augmented to include additional features for the modeling process. Summaries for the development of the features can be seen below:

- **Quote Count:** A sum of the total words contained within the text of the quote.
- **Text Combinations:** All permutations of the *text*, *author* and *tags* columns for model experimentation to determine which combination would provide the most favorable results.
- **Sentiment Score:** A sentiment score determined by a scoring function from the project [qary](https://gitlab.com/tangibleai/qary), which is based on the NLTK vader [SentimentIntensityAnalyzer](https://www.nltk.org/api/nltk.sentiment.html#nltk.sentiment.vader.SentimentIntensityAnalyzer). A low sentiment score tends to be a more negative sentiment, a higher score tends to be a more positive sentiment, while a score closer to zero can be interpreted as having neutral sentiment.
- **Positive Similarity Score:** A similarity score of the comparison between the appropriate text combination (mentioned above) of the given quote and all previously provided quotes that were *liked* by the user. The similarity score was determined by finding the cosine similarity between the texts using the word embeddings from [spaCy](https://spacy.io/). The similarities between each of the quotes were then averaged to provide the final result.
- **Negative Similarity Score:** A similarity score akin to the *Positive Similairity Score*, with the only difference being it compared the appropriate text combination of the given quote and all previously provided quotes that were *disliked* by the user.

Additionally, there were no quote ratings provided with the original data set, whether in the form of quote popularity or individual user rankings. To solve this problem, a modeling data set was curated by using the `modeling_data_builder.py` program within this project. This included randomly providing quotes to a test user and recording their feedback until just over two-hundred quotes had been rated. The results were recorded to the `modeling_data.csv` data set. It is worth mentioning the target class - *the user_feedback*  column - was largely imbalanced after collecting the data, with nearly double the the amount of *liked* quotes compared to *disliked* quotes. This sample is assumed to mimic the average real world scenario of the quote recommendation program.

#### Methods of Analysis
The target variable to predict for this project is whether a user will like a given quote or not, represented in the form of a binary classification problem. The models chosen to be explored were Logistic Regression, K-Nearest Neighbors, Support Vector Machine and Decision Tree classifiers. Various hyperparameters were manually tuned in attempts to obtain the best results. Additionally, different combinations of features were explored, beginning with a single feature and adding others to gauge the performance.

The metrics chosen to assess the models' performance were:
- **Accuracy:** Chosen to gauge the overall performance of the model relative to the baseline percentages of the target classes.
- **Precision:** Chosen to exploit the true performance of the model due to the heavily imbalanced target classes in the modeling data set. Also, this represents how many positive predictions were actually correct. The CLI program chooses only positive predictions to give back to the user, so the precision metric should be maximized.
- **False Positive Ratio (FP):** A false positive prediction would be the worst outcome to present to the user, so the false positive ratio was aimed to be minimized during the modeling process. This is represented as a ratio of the false positives to the total number of predictions made, or: \
`FP / (TP + FP + TN + FN)`

#### Results
The Logistic Regression model proved to be the most useful for two main reasons:
1. It provided the best results with minimal overfitting.
2. It was the least computationally expensive option of the models.

The second point is important for the CLI program because it has to retrain the model after every entry of user feedback. The minimalist Logistic Regression model should cut back processing time, especially as the user history grows in data points.

The models struggled to improve upon the baseline accuracies, which were 63.6% for liked quotes and 36.4% for disliked quotes. The highest accuracy obtained from the model was ~66% on the testing data, and ~72% on the training data. However, this model was abandoned for a model with slightly less accuracy but higher precision. It also had a lower false positive ratio.

The results of the chosen Logistic Regression model are summarized below:

|**Model**|**Parameters**|**Features**|**Training Accuracy**|**Testing Accuracy**|**Precision**|**FP%**|
|---|---|---|---|---|---|---|
|Logistic Regression|C: 0.1; solver: lbfgs; penalty: l2; class_weight: balanced; scaling: n; max_iter: 5000|quote_word_count, pos_sim, neg_sim|0.6389|0.6451|0.7429|0.1451|
>Results from favored Logistic Regression model.

|**Feature**|**Coefficient**|**P Value**|
|---|---|---|
|**quote_word_count**|-.0081|0.001|
|**pos_sim**|36.94|0.003|
|**neg_sim**|-34.5|0.005|
>Model summary statistics.

Interestingly, the best performing model was one without the sentiment analysis feature. After investigating some of the quotes with differing sentiment scores, it was understandable why this feature may not be a good predictor for likeability. For instance, some quotes with low sentiment scores - or more negative sentiment - had humorous overtones the NLP techniques and model did not pick up on.

As intuition would suggest, the positive and negative similarity features proved to provide the most predictive power for quote recommendations. This is represented in the feature coefficients of the table above, showing equal but opposite quantities. Additionally, all features had low p-values, indicating all features provided significant relationships with predicting the target variable.

## CLI Program
The user interface for interacting with the recommendation engine is done via the command line in chatbot form. The conversation accommodates four states, which are shown below, along with the conversation design flow:
1. Introduction or greeting.
2. Waiting.
3. Providing a quote and receiving feedback from user regarding the quote.
4. Farewell or exit.

<p align="center">
<img src="./images/cli-flow-chart.png" width="400"/>
</p>

Once the user requests a quote, the program randomly samples five quotes that have not been provided to that user before. The negative and positive similarity scores are calculated, the model is retrained on the previously provided data, and then the five quotes are fed into the model for predictions. Of the five, the quote predicted with the highest probability of being liked by the user is presented to them. The reply from the chatbot includes the quote's text, author and categories as shown in the example below:

```
Cherish sunsets, wild creatures and wild places. Have a love affair with the wonder and beauty of the earth.
-Stewart Udall  #Love #Beauty #Earth #Places

Let me know if you like that one and I'll keep trying to find quotes you like! (y/n)
```

If the user chooses to respond as either liking or disliking the quote (y/n), that quote and response (or row of data) is appended to a global history data frame for retraining the model.

## Future Work
As the product seen here represents the MVP, there is a lot more work to be done to flush out the recommendation engine and the user interface. A summary can be seen below:

**Recommendation Engine Improvements:**
- **Hybrid User + Content Based Recommendation Engine:**\
With enough feedback from different users, the data could be incorporated with the content-based system to build a hybrid engine. This has potential to vastly improve predictability in recommending quotes to users.

- **More Sophisticated NLP:**\
The NLP utilized included word embeddings and sentiment analysis, and these methods were not taking advantage of the order of words in the sentences. More sophisticated NLP techniques like sentence transformers could uncover deeper differences or similarities between quotes, ultimately providing improved predictive power.\
\
Another improvement would be to utilize the user's previous conversations with the chatbot as part of the content-based portion of the engine. It stands to reason that quotes with similar language or semantics to the user's own words would be more appealing to them.

**CLI Improvements**
- **Incorporate into qary:**\
Now that the conversation and modeling flow has been worked out, the next step is to incorporate this interface into the qary chatbot framework as a skill.

- **More Sophisticated NLU:**\
Currently, the chatbot detects keywords in the user's response to determine how to respond and is very limited in what it can understand. Implementing more sophisticated NLU to interpret the intent of the user could go a long way in making a more conversant chatbot.

- **Request Type and Number of Quotes:**\
The MVP only allows the user to request a single quote at a time, which can get cumbersome if you're trying to find that perfect quote. The small implementation of allowing them to request the type and quantity of quotes could noticeably improve the user experience. For example, asking the chatbot for "3 political quotes" would produce three quotes having political-ness.

## Conclusions

With a marginal, nearly negligible, improvement of ~1% on the baseline accuracy (jumping from 63.6% to 64.5%), the MVP appears to fall short of remarkable recommendations. However, considering the CLI program chooses only positive predictions to relay back to the user means the precision and false positive ratio metrics must also be considered. The precision and false positive ratios for the chosen model were 74.5% and 14.5%, respectively. This means the user will receive a false positive prediction roughly one out of every four times, or 25% of the time. Although this is not ideal, it may be acceptable for an MVP. With this information, combined with the next steps of improving the NLP and incorporating a user-based engine, it demonstrates there may be a viable path for an improved quote recommendation engine in the future.

In experimenting with the quote recommender, it provided me with the quote below that serves as inspiration for me to continue the path forward on this project. All science aside, the empirical evidence shows this engine has the capability to achieve its ultimate goal: benefitting the user.
```
I am always doing that which I cannot do, in order that I may learn how to do it.
-Pablo Picasso  #Learning #Doing
```
## Sources

**Original Quote Data Set:**\
https://github.com/MartinStyk/quotes-recommender

**Sentiment Analysis:**\
https://gitlab.com/tangibleai/qary

**CSV to Markdown Coverter for Hyperparameter Tuning Table:**\
https://csvtomd.com/#/

## Appendix

#### Hyperparameter Tuning Table
| model               | features                                            | parameters                                                                                                                                               | baseline_acc [1,0] | train_acc          | test_acc           | recall              | precision          | f1_score           | fp_%                |
| ------------------- | --------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------ | ------------------ | ------------------ | ------------------- | ------------------ | ------------------ | ------------------- |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 1;             solver: lbfgs;             penalty: l2;             class_weight: None;             scaling: n;             bootstrap: n               | 0.855, 0.145       | 0.6388888888888888 | 0.6129032258064516 | 0.8717948717948718  | 0.6415094339622641 | 0.7391304347826088 | 0.3064516129032258  |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.5;             solver: lbfgs;             penalty: l2;             class_weight: None;             scaling: n;             bootstrap: n             | 0.823, 0.177       | 0.6388888888888888 | 0.6129032258064516 | 0.8461538461538461  | 0.6470588235294118 | 0.7333333333333334 | 0.2903225806451613  |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.1;             solver: lbfgs;             penalty: l2;             class_weight: None;             scaling: n;             bootstrap: n             | 0.806, 0.194       | 0.6875             | 0.6612903225806451 | 0.8717948717948718  | 0.68               | 0.7640449438202247 | 0.25806451612903225 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.01;             solver: lbfgs;             penalty: l2;             class_weight: None;             scaling: n;             bootstrap: n            | 0.823, 0.177       | 0.6736111111111112 | 0.6774193548387096 | 0.8974358974358975  | 0.6862745098039216 | 0.7777777777777778 | 0.25806451612903225 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 1;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: n;             bootstrap: n           | 0.548, 0.452       | 0.6388888888888888 | 0.5967741935483871 | 0.6153846153846154  | 0.7058823529411765 | 0.6575342465753424 | 0.16129032258064516 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.5;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: n;             bootstrap: n         | 0.581, 0.419       | 0.6527777777777778 | 0.6290322580645161 | 0.6666666666666666  | 0.7222222222222222 | 0.6933333333333334 | 0.16129032258064516 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.1;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: n;             bootstrap: n         | 0.548, 0.452       | 0.6458333333333334 | 0.6290322580645161 | 0.6410256410256411  | 0.7352941176470589 | 0.6849315068493151 | 0.14516129032258066 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.01;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: n;             bootstrap: n        | 0.565, 0.435       | 0.6388888888888888 | 0.6451612903225806 | 0.6666666666666666  | 0.7428571428571429 | 0.7027027027027027 | 0.14516129032258066 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.001;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: n;             bootstrap: n       | 0.565, 0.435       | 0.6388888888888888 | 0.6451612903225806 | 0.6666666666666666  | 0.7428571428571429 | 0.7027027027027027 | 0.14516129032258066 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.001;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: y;             bootstrap: n       | 0.532, 0.468       | 0.6319444444444444 | 0.5806451612903226 | 0.5897435897435898  | 0.696969696969697  | 0.638888888888889  | 0.16129032258064516 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.1;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: y;             bootstrap: n         | 0.565, 0.435       | 0.6597222222222222 | 0.6129032258064516 | 0.6410256410256411  | 0.7142857142857143 | 0.6756756756756757 | 0.16129032258064516 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 1;             solver: saga;             penalty: elasticnet;             class_weight: balanced;             scaling: n;             bootstrap: n    | 0, 1               | 0.3611111111111111 | 0.3709677419354839 | 0.0                 | 0.0                | 0.0                | 0.0                 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 1;             solver: saga;             penalty: elasticnet;             class_weight: balanced;             scaling: n;             bootstrap: n    | 0, 1               | 0.3611111111111111 | 0.3709677419354839 | 0.0                 | 0.0                | 0.0                | 0.0                 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 1;             solver: saga;             penalty: elasticnet;             class_weight: balanced;             scaling: y;             bootstrap: n    | 0.645, 0.355       | 0.7083333333333334 | 0.6612903225806451 | 0.7435897435897436  | 0.725              | 0.7341772151898733 | 0.1774193548387097  |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 1;             solver: saga;             penalty: elasticnet;             class_weight: balanced;             scaling: y;             bootstrap: n    | 0.645, 0.355       | 0.7013888888888888 | 0.6612903225806451 | 0.7435897435897436  | 0.725              | 0.7341772151898733 | 0.1774193548387097  |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.1;             solver: saga;             penalty: elasticnet;             class_weight: balanced;             scaling: y;             bootstrap: n  | 0.565, 0.435       | 0.6597222222222222 | 0.6129032258064516 | 0.6410256410256411  | 0.7142857142857143 | 0.6756756756756757 | 0.16129032258064516 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.01;             solver: saga;             penalty: elasticnet;             class_weight: balanced;             scaling: y;             bootstrap: n | 0.516, 0.484       | 0.6388888888888888 | 0.5967741935483871 | 0.5897435897435898  | 0.71875            | 0.6478873239436619 | 0.14516129032258066 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.01;             solver: saga;             penalty: elasticnet;             class_weight: balanced;             scaling: y;             bootstrap: n | 1, 0               | 0.6388888888888888 | 0.6290322580645161 | 1.0                 | 0.6290322580645161 | 0.7722772277227723 | 0.3709677419354839  |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 1;             solver: saga;             penalty: l1;             class_weight: balanced;             scaling: n;             max_iter: 1000          | 0.597, 0.403       | 0.5902777777777778 | 0.5483870967741935 | 0.46153846153846156 | 0.72               | 0.5625             | 0.11290322580645161 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 1;             solver: saga;             penalty: l1;             class_weight: balanced;             scaling: n;             max_iter: 2000          | 0.5, 0.5           | 0.6597222222222222 | 0.5806451612903226 | 0.5641025641025641  | 0.7096774193548387 | 0.6285714285714286 | 0.14516129032258066 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 1;             solver: saga;             penalty: l1;             class_weight: balanced;             scaling: n;             max_iter: 5000          | 0.532, 0.468       | 0.6319444444444444 | 0.6129032258064516 | 0.6153846153846154  | 0.7272727272727273 | 0.6666666666666667 | 0.14516129032258066 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 1;             solver: liblinear;             penalty: l1;             class_weight: balanced;             scaling: n;             max_iter: 5000     | 0.548, 0.452       | 0.6319444444444444 | 0.5967741935483871 | 0.6153846153846154  | 0.7058823529411765 | 0.6575342465753424 | 0.16129032258064516 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.5;             solver: saga;             penalty: l1;             class_weight: balanced;             scaling: n;             max_iter: 5000        | 0.516, 0.484       | 0.6319444444444444 | 0.5967741935483871 | 0.5897435897435898  | 0.71875            | 0.6478873239436619 | 0.14516129032258066 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.1;             solver: saga;             penalty: l1;             class_weight: balanced;             scaling: n;             max_iter: 5000        | 0, 1               | 0.3611111111111111 | 0.3709677419354839 | 0.0                 | 0.0                | 0.0                | 0.0                 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.1;             solver: saga;             penalty: l1;             class_weight: balanced;             scaling: y;             max_iter: 5000        | 0.565, 0.435       | 0.6458333333333334 | 0.6129032258064516 | 0.6410256410256411  | 0.7142857142857143 | 0.6756756756756757 | 0.16129032258064516 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.1;             solver: liblinear;             penalty: l1;             class_weight: balanced;             scaling: n;             max_iter: 5000   | 0, 1               | 0.3611111111111111 | 0.3709677419354839 | 0.0                 | 0.0                | 0.0                | 0.0                 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.1;             solver: liblinear;             penalty: l1;             class_weight: balanced;             scaling: y;             max_iter: 5000   | 0.532, 0.468       | 0.6388888888888888 | 0.6129032258064516 | 0.6153846153846154  | 0.7272727272727273 | 0.6666666666666667 | 0.14516129032258066 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.01;             solver: liblinear;             penalty: l1;             class_weight: balanced;             scaling: y;             max_iter: 5000  | 0, 1               | 0.3611111111111111 | 0.3709677419354839 | 0.0                 | 0.0                | 0.0                | 0.0                 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.01;             solver: liblinear;             penalty: l1;             class_weight: balanced;             scaling: n;             max_iter: 5000  | 0, 1               | 0.3611111111111111 | 0.3709677419354839 | 0.0                 | 0.0                | 0.0                | 0.0                 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.001;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: n;             max_iter: 5000     | 0.565, 0.435       | 0.6388888888888888 | 0.6451612903225806 | 0.6666666666666666  | 0.7428571428571429 | 0.7027027027027027 | 0.14516129032258066 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 1;             solver: lbfgs;             penalty: none;             class_weight: balanced;             scaling: n;             max_iter: 5000       | 0.645, 0.355       | 0.7222222222222222 | 0.6612903225806451 | 0.7435897435897436  | 0.725              | 0.7341772151898733 | 0.1774193548387097  |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.01;             solver: lbfgs;             penalty: none;             class_weight: balanced;             scaling: n;             max_iter: 5000    | 0.645, 0.355       | 0.7222222222222222 | 0.6612903225806451 | 0.7435897435897436  | 0.725              | 0.7341772151898733 | 0.1774193548387097  |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 1;             solver: lbfgs;             penalty: none;             class_weight: balanced;             scaling: n;             max_iter: 5000       | 0.645, 0.355       | 0.7222222222222222 | 0.6612903225806451 | 0.7435897435897436  | 0.725              | 0.7341772151898733 | 0.1774193548387097  |
| Logistic Regression | pos_sim                                             | C: 0.1;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: n;             max_iter: 5000       | 0.694, 0.306       | 0.4861111111111111 | 0.3870967741935484 | 0.2564102564102564  | 0.5263157894736842 | 0.3448275862068965 | 0.14516129032258066 |
| Logistic Regression | pos_sim, neg_sim                                    | C: 0.1;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: n;             max_iter: 5000       | 0.677, 0.323       | 0.4791666666666667 | 0.3709677419354839 | 0.2564102564102564  | 0.5                | 0.3389830508474576 | 0.16129032258064516 |
| Logistic Regression | pos_sim, neg_sim, quote_word_count                  | C: 0.1;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: n;             max_iter: 5000       | 0.565, 0.435       | 0.6388888888888888 | 0.6451612903225806 | 0.6666666666666666  | 0.7428571428571429 | 0.7027027027027027 | 0.14516129032258066 |
| Logistic Regression | pos_sim, quote_word_count                           | C: 0.1;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: n;             max_iter: 5000       | 0.565, 0.435       | 0.6388888888888888 | 0.6451612903225806 | 0.6666666666666666  | 0.7428571428571429 | 0.7027027027027027 | 0.14516129032258066 |
| Logistic Regression | pos_sim, quote_word_count, sentiment_score          | C: 0.1;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: n;             max_iter: 5000       | 0.548, 0.452       | 0.6458333333333334 | 0.6290322580645161 | 0.6410256410256411  | 0.7352941176470589 | 0.6849315068493151 | 0.14516129032258066 |
| Logistic Regression | pos_sim, quote_word_count, sentiment_score          | C: 0.1;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: y;             max_iter: 5000       | 0.613, 0.387       | 0.6388888888888888 | 0.6612903225806451 | 0.717948717948718   | 0.7368421052631579 | 0.7272727272727273 | 0.16129032258064516 |
| Logistic Regression | pos_sim, quote_word_count, sentiment_score          | C: 0.1;             solver: lbfgs;             penalty: none;             class_weight: balanced;             scaling: y;             max_iter: 5000     | 0.629, 0.371       | 0.6736111111111112 | 0.6451612903225806 | 0.717948717948718   | 0.717948717948718  | 0.717948717948718  | 0.1774193548387097  |
| Logistic Regression | pos_sim, quote_word_count, sentiment_score          | C: 0.1;             solver: lbfgs;             penalty: none;             class_weight: balanced;             scaling: n;             max_iter: 5000     | 0.629, 0.371       | 0.6736111111111112 | 0.6451612903225806 | 0.717948717948718   | 0.717948717948718  | 0.717948717948718  | 0.1774193548387097  |
| Logistic Regression | quote_word_count                                    | C: 0.1;             solver: lbfgs;             penalty: none;             class_weight: balanced;             scaling: n;             max_iter: 5000     | 0.565, 0.435       | 0.6388888888888888 | 0.6451612903225806 | 0.6666666666666666  | 0.7428571428571429 | 0.7027027027027027 | 0.14516129032258066 |
| Logistic Regression | quote_word_count                                    | C: 0.1;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: n;             max_iter: 5000       | 0.565, 0.435       | 0.6388888888888888 | 0.6451612903225806 | 0.6666666666666666  | 0.7428571428571429 | 0.7027027027027027 | 0.14516129032258066 |
| Logistic Regression | quote_word_count                                    | C: 0.01;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: n;             max_iter: 5000      | 0.565, 0.435       | 0.6388888888888888 | 0.6451612903225806 | 0.6666666666666666  | 0.7428571428571429 | 0.7027027027027027 | 0.14516129032258066 |
| Logistic Regression | quote_word_count                                    | C: 0.01;             solver: lbfgs;             penalty: l2;             class_weight: none;             scaling: n;             max_iter: 5000          | 0.823, 0.177       | 0.6736111111111112 | 0.6774193548387096 | 0.8974358974358975  | 0.6862745098039216 | 0.7777777777777778 | 0.25806451612903225 |
| Logistic Regression | quote_word_count, sentiment_score                   | C: 0.01;             solver: lbfgs;             penalty: l2;             class_weight: none;             scaling: n;             max_iter: 5000          | 0.823, 0.177       | 0.6736111111111112 | 0.6774193548387096 | 0.8974358974358975  | 0.6862745098039216 | 0.7777777777777778 | 0.25806451612903225 |
| Logistic Regression | quote_word_count, sentiment_score                   | C: 0.1;             solver: lbfgs;             penalty: l2;             class_weight: none;             scaling: n;             max_iter: 5000           | 0.806, 0.194       | 0.6875             | 0.6612903225806451 | 0.8717948717948718  | 0.68               | 0.7640449438202247 | 0.25806451612903225 |
| Logistic Regression | quote_word_count, sentiment_score                   | C: 0.1;             solver: lbfgs;             penalty: l2;             class_weight: none;             scaling: y;             max_iter: 5000           | 0.903, 0.097       | 0.6527777777777778 | 0.5967741935483871 | 0.8974358974358975  | 0.625              | 0.736842105263158  | 0.3387096774193548  |
| Logistic Regression | quote_word_count, sentiment_score, pos_sim          | C: 0.1;             solver: lbfgs;             penalty: l2;             class_weight: none;             scaling: n;             max_iter: 5000           | 0.806, 0.194       | 0.6875             | 0.6612903225806451 | 0.8717948717948718  | 0.68               | 0.7640449438202247 | 0.25806451612903225 |
| Logistic Regression | quote_word_count, sentiment_score, pos_sim          | C: 0.1;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: n;             max_iter: 5000       | 0.548, 0.452       | 0.6458333333333334 | 0.6290322580645161 | 0.6410256410256411  | 0.7352941176470589 | 0.6849315068493151 | 0.14516129032258066 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.1;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: n;             max_iter: 5000       | 0.548, 0.452       | 0.6458333333333334 | 0.6290322580645161 | 0.6410256410256411  | 0.7352941176470589 | 0.6849315068493151 | 0.14516129032258066 |
| Logistic Regression | quote_word_count, sentiment_score, neg_sim, pos_sim | C: 0.1;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: n;             max_iter: 5000       | 0.548, 0.452       | 0.6458333333333334 | 0.6290322580645161 | 0.6410256410256411  | 0.7352941176470589 | 0.6849315068493151 | 0.14516129032258066 |
| Logistic Regression | quote_word_count, pos_sim, neg_sim                  | C: 0.1;             solver: lbfgs;             penalty: l2;             class_weight: balanced;             scaling: n;             max_iter: 5000       | 0.565, 0.435       | 0.6388888888888888 | 0.6451612903225806 | 0.6666666666666666  | 0.7428571428571429 | 0.7027027027027027 | 0.14516129032258066 |
|                     |
