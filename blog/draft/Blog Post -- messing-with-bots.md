# Messing with Bots

Messing with bots is like torturing invasive species insects like fire ants ... you can get out your agression while doing something prosocial.

## Capchas

- incorrectly but not too incorrectly
- chose the ones you think a bot would select
- do it 2 or 3 times before answering correctly

## Map Navigation

- once you get directions turn down volume
- memorize route and take alternate shortcuts
- drive more slowly and more carefully sometimes (at night)
- when asked about speed traps wait until late to say yes, or no, randomly
- after route complete give oposite feedback on quality
- give false information about "is it still here"
- look for things to complain about for SEO buseinsses that search recommends (if they deserve it)
- advanced: stop and park long before destination restaurants and eat at a non SEO business
- advanced: transfer information about prosocial, non-SEO businesses to OSM

## Voice Assistants

- advanced: train wake word to recognize derogatory phraing (Hello Giggle, See Me or Sue Me, Crapazon)
- mess with it by using a lot of homynyms, homophones, obscure slang, technical terms, baby talk
- search for vegetarian, prosocial options before falling back to your preferred option
- use wikipedia search often
- provide negative feedback often

## Spam calls/emails

- ask them about their product, then how the caller uses the product, then personal details like where they live and eventually ask for their phone number so you can call them back and place your order
- schmooze the scammer, flirt with them or flatter them using "The Power of Influence" techniques or "Impossible Conversations" or trying out your latest jokes, sarcasms or memes
- advanced: have your voice assistant converse with spam callers
- advanced: con the scammer, play along giving them fake information, including the phone number to a congressman on the FCC committee or the FCC agents themselves or the FBI interstate fraud division.
- scam relay - give them a random number from your block list as your telephone number and the street address of an FBI agent or your neighborhood FedEx or Amazon distribution center or office

## information should be free

- pirate academic journals and textbooks
- self-host videos rather than YouTube

## Search Engines

- use Duck.com
- use Wikipedia Search bar directly
- put prosocial queries (including nonprofit org names) in the big search engines
- Skip the first 5 or 10 or 100 links in any big 3 search engine
- use search engines and large retailer sites to research books, movies, and electronics, then purchase them at your local store or smaller online retailers

# Less fun, less botty

## Jailbreak the LockIn

- use open source cross-platform cloud tools like boto3, Docker, Terraform, and Kubernetes
- insert UX flaws into open source projects corrupted by big tech (VS Code, GitHub, Ubuntu)
- Digital Ocean, Heroku, Linode, and other smaller cloud providers
- GitLab instead of GitHub
- Sublime Text instead of VSCode or XCode
- Linux instead of Mac or Windows
- substack instead of twitter, facebook, linkedIn
- use your own shorturl


